package maestro.goodcalendar

import java.util.*

/**
 * Created by maestro123 on 4/19/2018.
 */
data class SelectedDate(val year: Int, val month: Int, val day: Int) {

    companion object {
        val SAME = 0
        val GRATER = 1
        val LOWER = 2

        fun fromDate(date: Date?): SelectedDate? {
            if (date != null) {
                val calendar = Calendar.getInstance()
                calendar.time = date
                return SelectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
            }
            return null
        }
    }

    fun match(year: Int, month: Int, day: Int): Boolean {
        return this.year == year && this.month == month && this.day == day
    }

    fun compare(year: Int, month: Int, day: Int = -1): Int =
            when {
                this.year > year -> GRATER
                this.year < year -> LOWER
                this.month > month -> GRATER
                this.month < month -> LOWER
                day > -1 && this.day > day -> GRATER
                day > -1 && this.day < day -> LOWER
                else -> SAME
            }

    fun isGraterOrSame(year: Int, month: Int, day: Int): Boolean {
        val cmp = compare(year, month, day)
        return cmp == GRATER || cmp == SAME
    }

    fun isLowerOrSame(year: Int, month: Int, day: Int): Boolean {
        val cmp = compare(year, month, day)
        return cmp == LOWER || cmp == SAME
    }

    fun time(endOfDay: Boolean): Long {
        val calendar = Calendar.getInstance()
        if (!endOfDay) {
            calendar.set(year, month, day, 0, 0, 0)
        } else {
            calendar.set(year, month, day, 23, 59, 59)
        }
        return calendar.timeInMillis
    }

}