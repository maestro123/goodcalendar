package maestro.goodcalendar

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet

class CalendarWeekDayTitleView : AppCompatTextView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

}