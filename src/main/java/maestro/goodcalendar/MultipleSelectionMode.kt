package maestro.goodcalendar

import java.util.*

/**
 * Created by maestro123 on 4/19/2018.
 */
class MultipleSelectionMode(calendarView: GoodCalendar) : SelectionMode(calendarView) {

    private val dates = mutableListOf<SelectedDate>()

    override fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean {
        if (checked) {
            dates.remove(SelectedDate(year, month, day))
            calendarView.syncCheckedState()
            return false
        }
        dates.add(SelectedDate(year, month, day))
        calendarView.syncCheckedState()
        return true
    }

    override fun isChecked(year: Int, month: Int, day: Int): Boolean {
        dates.forEach { if (it.match(year, month, day)) return true }
        return false
    }

    fun setSelected(date: Date?) {
        if (date == null) return
        SelectedDate.fromDate(date)?.let {
            if (!dates.contains(it)) {
                dates.add(it)
                calendarView.syncCheckedState()
            }
        }
    }

    fun removeDate(date: Date?) {
        if (date == null) return
        SelectedDate.fromDate(date)?.let {
            val index = dates.indexOf(it)
            if (index != -1) {
                dates.removeAt(index)
                calendarView.syncCheckedState()
            }
        }
    }

    fun clear() {
        if (dates.size > 0) {
            dates.clear()
            calendarView.syncCheckedState()
        }
    }

    fun getSelectedDates(): List<SelectedDate> = dates.toList()

}