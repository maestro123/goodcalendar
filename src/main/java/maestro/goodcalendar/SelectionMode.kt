package maestro.goodcalendar

/**
 * Created by maestro123 on 4/19/2018.
 */

abstract class SelectionMode(val calendarView: GoodCalendar) {

    abstract fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean

    abstract fun isChecked(year: Int, month: Int, day: Int): Boolean

}