package maestro.goodcalendar

import android.annotation.TargetApi
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.StateListDrawable
import android.graphics.drawable.shapes.OvalShape
import android.os.Build
import android.support.v7.widget.AppCompatCheckedTextView
import android.util.AttributeSet
import android.view.View

/**
 * Created by maestro123 on 4/19/2018.
 */
class CalendarDayView : AppCompatCheckedTextView {

    private val path = Path()
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val hotspotBounds = Rect()
    private var customBackground: StateListDrawable? = null

    private var currentColor = -1
        set(value) {
            field = value
            paint.color = value
        }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    fun prepareBackground(color: Int) {
        currentColor = color
        if (customBackground == null) {
            if (background == null) {
                customBackground = generateBackground(color,
                        minOf(75, resources.getInteger(android.R.integer.config_shortAnimTime)))
                setBackgroundDrawable(customBackground)
                return
            }
        }
        customBackground?.let {
            getDrawableForState(intArrayOf(android.R.attr.state_checked))?.let {
                if (it is ShapeDrawable) {
                    it.paint.color = color
                } else {
                    it.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                }
            }
            getDrawableForState(intArrayOf(android.R.attr.state_pressed))?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && it is RippleDrawable) {
                    it.setColor(ColorStateList.valueOf(color))
                } else if (it is ShapeDrawable) {
                    it.paint.color = color
                } else {
                    it.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                }
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas == null) return
        if (isChecked) {
            (parent as? CalendarMonthView)?.let {
                path.reset()
                it.buildDayPath(this, path)
                canvas.drawPath(path, paint)
            }
        }
        super.onDraw(canvas)
    }

    private fun getDrawableForState(currentState: IntArray): Drawable? {
        val getStateDrawableIndex = StateListDrawable::class.java.getMethod("getStateDrawableIndex", IntArray::class.java)
        val getStateDrawable = StateListDrawable::class.java.getMethod("getStateDrawable", Int::class.java);
        val index = getStateDrawableIndex.invoke(customBackground, currentState);
        return getStateDrawable.invoke(customBackground, index) as Drawable
    }

    private fun generateBackground(color: Int, fadeTime: Int): StateListDrawable {
        val drawable = StateListDrawable()
        drawable.setExitFadeDuration(fadeTime)
        drawable.addState(intArrayOf(android.R.attr.state_checked), generateCircleDrawable(color))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable.addState(intArrayOf(android.R.attr.state_pressed), generateRippleDrawable(color, hotspotBounds))
        } else {
            drawable.addState(intArrayOf(android.R.attr.state_pressed), generateCircleDrawable(color))
        }
        drawable.addState(intArrayOf(), generateCircleDrawable(Color.TRANSPARENT))
        return drawable
    }

    private fun generateCircleDrawable(color: Int): Drawable {
        val drawable = ShapeDrawable(OvalShape())
        drawable.paint.color = color
        return drawable
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun generateRippleDrawable(color: Int, bounds: Rect): Drawable {
        val list = ColorStateList.valueOf(color)
        val mask = generateCircleDrawable(Color.WHITE)
        val rippleDrawable = RippleDrawable(list, null, mask)
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            rippleDrawable.bounds = bounds
        }
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            val center = (bounds.left + bounds.right) / 2
            rippleDrawable.setHotspotBounds(center, bounds.top, center, bounds.bottom)
        }
        return rippleDrawable
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (customBackground != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            calculateBounds(w, h)
            val center = (hotspotBounds.left + hotspotBounds.right) / 2
            customBackground?.setHotspotBounds(center, hotspotBounds.top, center, hotspotBounds.bottom)
        }
    }

    private fun calculateBounds(width: Int, height: Int) {
        val radius = Math.min(height, width)
        val offset = Math.abs(height - width) / 2
        val circleOffset = if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) offset / 2 else offset
        if (width >= height) {
            hotspotBounds.set(circleOffset, 0, radius + circleOffset, height)
        } else {
            hotspotBounds.set(0, circleOffset, width, radius + circleOffset)
        }
    }

}