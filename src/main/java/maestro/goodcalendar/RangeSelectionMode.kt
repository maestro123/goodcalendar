package maestro.goodcalendar

import java.util.*

/**
 * Created by maestro123 on 4/19/2018.
 */
class RangeSelectionMode(calendarView: GoodCalendar) : SelectionMode(calendarView) {

    private var startDate: SelectedDate? = null
    private var endDate: SelectedDate? = null

    override fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean {
        if (startDate == null || endDate != null) {
            startDate = SelectedDate(year, month, day)
            endDate = null
            calendarView.syncCheckedState()
            return true
        }
        if (startDate != null) {
            if (startDate!!.match(year, month, day)) {
                startDate = null
                calendarView.syncCheckedState()
                return false
            }
            if (startDate!!.compare(year, month, day) == SelectedDate.LOWER) {
                endDate = startDate
                startDate = SelectedDate(year, month, day)
                calendarView.syncCheckedState()
                return true
            } else {
                endDate = SelectedDate(year, month, day)
                calendarView.syncCheckedState()
                return true
            }
        }
        return false
    }

    override fun isChecked(year: Int, month: Int, day: Int): Boolean {
        if (endDate == null) {
            return startDate?.match(year, month, day) == true
        }
        return startDate?.isGraterOrSame(year, month, day) == true && endDate!!.isLowerOrSame(year, month, day)
    }

    fun setSelected(dateStart: Date?, dateEnd: Date?) {
        if (dateStart != null && dateEnd != null) {
            if (dateStart.before(dateEnd)) {
                startDate = SelectedDate.fromDate(dateEnd)
                endDate = SelectedDate.fromDate(dateStart)
            } else {
                startDate = SelectedDate.fromDate(dateStart)
                endDate = SelectedDate.fromDate(dateEnd)
            }
            calendarView.syncCheckedState()
        } else if (dateStart != null) {
            startDate = SelectedDate.fromDate(dateStart)
            endDate = null
            calendarView.syncCheckedState()
            calendarView.jumpToDate(startDate!!.year, startDate!!.month)
        } else if (dateEnd != null) {
            startDate = SelectedDate.fromDate(dateEnd)
            endDate = null
            calendarView.syncCheckedState()
        } else {
            startDate = null
            endDate = null
            calendarView.syncCheckedState()
        }
    }

    fun getSelectedDates(): Pair<SelectedDate?, SelectedDate?> = Pair(startDate, endDate)

}