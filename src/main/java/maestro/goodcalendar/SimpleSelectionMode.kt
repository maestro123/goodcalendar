package maestro.goodcalendar

import java.util.*

/**
 * Created by maestro123 on 4/19/2018.
 */
open class SimpleSelectionMode(calendarView: GoodCalendar) : SelectionMode(calendarView) {

    private var currentSelectedDate: SelectedDate? = null

    override fun isChecked(year: Int, month: Int, day: Int): Boolean {
        return currentSelectedDate?.match(year, month, day) == true
    }

    override fun onDayCheckChange(year: Int, month: Int, day: Int, checked: Boolean): Boolean {
        if (!checked) {
            currentSelectedDate = SelectedDate(year, month, day)
            calendarView.syncCheckedState()
        } else {
            currentSelectedDate = null
            calendarView.syncCheckedState()
        }
        return !checked
    }

    fun setSelectedDate(date: Date?) {
        if (date != null) {
            currentSelectedDate = SelectedDate.fromDate(date)
            calendarView.syncCheckedState()
        } else if (currentSelectedDate != null) {
            currentSelectedDate = null
            calendarView.syncCheckedState()
        }
    }

    fun getSelectedDate(): SelectedDate? = currentSelectedDate

}