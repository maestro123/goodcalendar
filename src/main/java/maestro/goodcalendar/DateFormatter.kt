package maestro.goodcalendar

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by maestro123 on 4/19/2018.
 */
open class DateFormatter {

    val calendar: Calendar = Calendar.getInstance()

    val defFormatter by lazy {
        SimpleDateFormat("LLLL yyyy", Locale.getDefault())
    }

    fun formatMonthTitle(date: Date): CharSequence = defFormatter.format(date)

    fun formatWeekDayTitle(dayOfWeek: Int): CharSequence {
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek)
        return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())
    }

}