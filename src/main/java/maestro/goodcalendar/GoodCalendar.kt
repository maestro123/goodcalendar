package maestro.goodcalendar

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.MotionEventCompat
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.util.TypedValue
import android.view.*
import android.widget.ImageView
import java.util.*

/**
 * Created by a.barouski on 8/30/2017.
 */
class GoodCalendar : ViewGroup {

    companion object {
        private val PENDING_NONE = 0
        private val PENDING_NEXT = 1
        private val PENDING_PREVIOUS = 2

        private val calendar = Calendar.getInstance()!!
    }

    var calendarsCount = 1
        set(value) {
            field = value
            requestLayout()
            minCalendarCache = calendarsCount + 2
        }

    var minCalendarCache = calendarsCount + 2
        private set

    var formatter = DateFormatter()
        set(value) {
            field = value
            //TODO: synchronize?
        }

    var calendarSpace = 0
        set(value) {
            field = value
            requestLayout()
        }

    var monthTitleSpace = 0
        set(value) {
            field = value
            requestLayout()
        }

    var weekDaysTitleSpace = 0
        set(value) {
            field = value
            requestLayout()
        }

    var monthTitleLayout = R.layout.def_calendar_month_title_view
        private set
    var weekDayTitleLayout = R.layout.def_calendar_week_day_title_view
        private set
    var dayLayout = R.layout.def_calendar_day_view
        private set
    var arrowLayout = R.layout.def_calendar_switch_button_view
        private set

    var leftArrow: ImageView? = null
        private set
    var rightArrow: ImageView? = null
        private set

    var mode: SelectionMode = MultipleSelectionMode(this)//RangeSelectionMode(this)//SimpleSelectionMode(this)
        set(value) {
            field = value
            syncCheckedState()
        }

    var tintColor = -1
        set(value) {
            field = value
            monthViews.forEach {
                it?.updateTintColor(this, value)
            }
        }

    private var maxDate: SelectedDate? = null
    private var minDate: SelectedDate? = null

    //0 - left scroll offset , size-1 -> right scroll offset
    private val monthViews by lazy { Array<CalendarMonthView?>(minCalendarCache, { i -> null }) }

    private lateinit var dragHelper: SimpleDragHelper

    private var canSlide = true
    private var isUnableToDrag = false

    private var pendingChange = PENDING_NONE

    private val longPressTimeOut: Long
    private var actionDownTime: Long = 0
    private var currentTranslationOffset = 0F
    private var initialMotionX: Float = 0.toFloat()
    private var initialMotionY: Float = 0.toFloat()
    private var isDisallowInterceptDispatched = false
    private var isDisallowInterceptTouchRequested = false

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        clipChildren = false
        clipToPadding = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            clipToOutline = false
        }

        dragHelper = SimpleDragHelper(context, this, dragHelperCallback)// DragHelper.create(this, 0.45F, mDragCallback)
        calendarSpace = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8F, resources.displayMetrics).toInt()
        longPressTimeOut = ViewConfiguration.getLongPressTimeout().toLong()
        monthTitleSpace = calendarSpace
        weekDaysTitleSpace = calendarSpace

        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.GoodCalendar)
            calendarsCount = array.getInt(R.styleable.GoodCalendar_calendarsCount, calendarsCount)
            calendarSpace = array.getDimensionPixelSize(R.styleable.GoodCalendar_calendarSpace, calendarSpace)
            monthTitleLayout = array.getResourceId(R.styleable.GoodCalendar_monthTitleLayout, monthTitleLayout)
            weekDayTitleLayout = array.getResourceId(R.styleable.GoodCalendar_weekDayTitleLayout, weekDayTitleLayout)
            dayLayout = array.getResourceId(R.styleable.GoodCalendar_dayLayout, dayLayout)
            monthTitleSpace = array.getDimensionPixelSize(R.styleable.GoodCalendar_monthTitleSpace, monthTitleSpace)
            weekDaysTitleSpace = array.getDimensionPixelSize(R.styleable.GoodCalendar_weekDaysTitleSpace, weekDaysTitleSpace)
            isResizeAllowed = array.getBoolean(R.styleable.GoodCalendar_allowResize, isResizeAllowed)
            tintColor = if (!array.hasValue(R.styleable.GoodCalendar_tintColor)) {
                getThemeAccentColor(context)
            } else {
                array.getColor(R.styleable.GoodCalendar_tintColor, 0)
            }
            array.recycle()
        }
        for (i in 0 until minCalendarCache) {
            monthViews[i] = CalendarMonthView(context).also {
                it.prepare(this)
                addView(it)
            }
        }

        with(LayoutInflater.from(context)) {
            leftArrow = (inflate(arrowLayout, this@GoodCalendar, false) as? ImageView)?.also {
                it.setOnClickListener { jumpPrevious() }
                addView(it)
            }
            rightArrow = (inflate(arrowLayout, this@GoodCalendar, false) as? ImageView)?.also {
                it.setOnClickListener { jumpNext() }
                addView(it)
            }
        }
        setArrowDrawables(
                ResourcesCompat.getDrawable(resources, R.drawable.ic_navigate_before_black_24dp, context.theme),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_navigte_forward_black_24dp, context.theme))

        resetCalendarAlphas(0F)
        prepareCalendar(monthViews[0], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 1)
        prepareCalendar(monthViews[1], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH))
        for (i in 2 until monthViews.size) {
            prepareCalendar(monthViews[i], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + i - 1)
        }
        syncArrows()
    }

    fun setArrowDrawables(previous: Drawable?, next: Drawable?) {
        leftArrow?.setImageDrawable(previous)
        rightArrow?.setImageDrawable(next)
    }

    fun setLimits(minDate: Date?, maxDate: Date?) {
        this.minDate = SelectedDate.fromDate(minDate)
        this.maxDate = SelectedDate.fromDate(maxDate)
        syncCheckedState()
    }

    fun jumpToDate(year: Int, month: Int) {
        if ((monthViews[1]!!.year >= year && monthViews[1]!!.month >= month)
                && (monthViews[monthViews.size - 2]!!.year <= year && monthViews[monthViews.size - 2]!!.month <= month))
            return

        calendar.set(year, month, 1)
        prepareCalendar(monthViews[0], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 1)
        prepareCalendar(monthViews[1], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH))
        for (i in 2 until monthViews.size) {
            prepareCalendar(monthViews[i], calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + i - 1)
        }
    }

    fun canScrollToNext(): Boolean {
        if (maxDate == null) return true
        val monthView = monthViews[1]!!
        return maxDate!!.compare(monthView.year, monthView.month) == SelectedDate.GRATER
    }

    fun canScrollToPrevious(): Boolean {
        if (minDate == null) return true
        val monthView = monthViews[1]!!
        return minDate!!.compare(monthView.year, monthView.month) == SelectedDate.LOWER
    }

    override fun dispatchDraw(canvas: Canvas?) {
        if (canvas == null) return
        canvas.save()
        canvas.translate(currentTranslationOffset, 0F)
        super.dispatchDraw(canvas)
        canvas.restore()
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (width == 0 || height == 0) return
        var left = paddingLeft
        val top = paddingTop
        monthViews.first()?.let {
            it.layout(left - it.measuredWidth - calendarSpace, top, left - calendarSpace, top + it.measuredHeight)
        }
        for (i in 1..calendarsCount) {
            monthViews[i]?.let {
                it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
                left += it.measuredWidth + calendarSpace
            }
        }
        monthViews.last()?.let {
            it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
        }
        leftArrow?.let {
            val top = monthViews[1]?.monthTitleView?.measuredHeight?.div(2)?.minus(it.measuredHeight / 2)?.plus(paddingTop)
                    ?: top
            val left = paddingLeft - it.measuredWidth / 2 + it.drawable.intrinsicWidth / 2
            it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
        }
        rightArrow?.let {
            val top = monthViews[monthViews.size - 2]?.monthTitleView?.measuredHeight?.div(2)?.minus(it.measuredHeight / 2)?.plus(paddingTop)
                    ?: top
            val left = width - paddingRight - it.measuredWidth / 2 - it.drawable.intrinsicWidth / 2
            it.layout(left, top, left + it.measuredWidth, top + it.measuredHeight)
        }
    }

    var isResizeAllowed = true
    var initialHeightMode: Int? = null

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (initialHeightMode == null) {
            initialHeightMode = heightMeasureSpec
        }
        var availableSize = MeasureSpec.getSize(widthMeasureSpec)
        if (MeasureSpec.getMode(initialHeightMode!!) == MeasureSpec.EXACTLY) {
            availableSize = minOf(availableSize, MeasureSpec.getSize(initialHeightMode!!) - paddingTop - paddingBottom)
        }
        if (availableSize > 0) {
            availableSize = (availableSize - paddingLeft - paddingRight - (calendarSpace * (calendarsCount - 1))) / calendarsCount
        }
        if (leftArrow != null) measureChild(leftArrow, widthMeasureSpec, heightMeasureSpec)
        if (rightArrow != null) measureChild(rightArrow, widthMeasureSpec, heightMeasureSpec)
        val measureResults = measure(widthMeasureSpec, heightMeasureSpec, availableSize)

        setMeasuredDimension(maxOf(measureResults[0], availableSize),
                if (MeasureSpec.getMode(initialHeightMode!!) != MeasureSpec.EXACTLY && (
                                MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY || (isResizeAllowed && currentTranslationOffset != 0F && dragHelper.dragState != SimpleDragHelper.STATE_IDLE))) MeasureSpec.getSize(heightMeasureSpec) else measureResults[1])
    }

    private val tmpArray = IntArray(2)

    private var initialLayoutParamsHeight: Int? = null

    private fun measure(widthMeasureSpec: Int, heightMeasureSpec: Int, availableSize: Int): IntArray {

        var maxWidth = 0
        var maxHeight = 0
        var totalWidth = 0
        monthViews.forEachIndexed { index, item ->
            item?.let {
                if (availableSize > 0) {
                    it.measure(MeasureSpec.makeMeasureSpec(availableSize, MeasureSpec.EXACTLY), heightMeasureSpec)
                } else {
                    measureChild(it, widthMeasureSpec, heightMeasureSpec)
                }
                if (index in 1..(calendarsCount - 1)) {
                    if (totalWidth > 0) {
                        totalWidth += calendarSpace
                    }
                    totalWidth += it.measuredWidth
                } else {
                    totalWidth = it.measuredWidth
                }
                maxWidth = maxOf(maxWidth, totalWidth)
                if (index > 0 && index < monthViews.size - 1) {
                    maxHeight = maxOf(maxHeight, it.measuredHeight)
                }
            }
        }
        tmpArray[0] = maxWidth + paddingLeft + paddingRight
        tmpArray[1] = maxHeight + paddingTop + paddingBottom
        return tmpArray
    }

    override fun requestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        super.requestDisallowInterceptTouchEvent(disallowIntercept)
        isDisallowInterceptTouchRequested = disallowIntercept
    }

    private var initialLayoutParams: LayoutParams? = null

    override fun setLayoutParams(params: LayoutParams?) {
        if (initialLayoutParams != params) {
            initialLayoutParams = params
            initialLayoutParamsHeight = null
            initialHeightMode = null
        }
        if (initialLayoutParamsHeight == null) {
            initialLayoutParamsHeight = params?.height
        }
        super.setLayoutParams(params)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        val action = MotionEventCompat.getActionMasked(ev)

        if (!canSlide || isUnableToDrag && action != MotionEvent.ACTION_DOWN) {
            dragHelper.cancel()
            return super.onInterceptTouchEvent(ev)
        }

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            dragHelper.cancel()
            return false
        }

        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x
                val y = ev.y
                isUnableToDrag = false
                initialMotionX = x
                initialMotionY = y
                actionDownTime = System.currentTimeMillis()
                isDisallowInterceptDispatched = false
                isDisallowInterceptTouchRequested = false
            }
            MotionEvent.ACTION_MOVE -> {
                if (isDisallowInterceptTouchRequested) {
                    isDisallowInterceptTouchRequested = false
                    return false
                }
                val x = ev.x
                val y = ev.y
                val adx = Math.abs(x - initialMotionX)
                val ady = Math.abs(y - initialMotionY)
                val slop = dragHelper.mTouchSlop
                if (adx > slop && ady > adx) {
                    dragHelper.cancel()
                    isUnableToDrag = true
                    return false
                }
            }
        }

        if (!isDisallowInterceptTouchRequested
                && dragHelper.dragState == SimpleDragHelper.STATE_DRAGGING) {
            requestDisallowInterceptTouchEvent(true)
        }

        return dragHelper.shouldInterceptTouchEvent(ev)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent): Boolean {
        if (!canSlide) return super.onTouchEvent(ev)
        if (ev.action == MotionEvent.ACTION_DOWN) {
            isDisallowInterceptDispatched = false
            isDisallowInterceptTouchRequested = false
        }
        dragHelper.processTouchEvent(ev)
        if (!isDisallowInterceptDispatched
                && dragHelper.dragState == SimpleDragHelper.STATE_DRAGGING) {
            requestDisallowInterceptTouchEvent(true)
        }
        return true
    }

    override fun computeScroll() {
        if (dragHelper.continueSettling(true)) {
            if (!canSlide) {
                dragHelper.abort()
                return
            }
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }

    fun syncCheckedState() = monthViews.forEach { it?.syncCheckedState(this) }

    private fun prepareCalendar(view: CalendarMonthView?, year: Int, month: Int) = view?.setup(this, year, month)

    private fun resetCalendarAlphas(alpha: Float) {
        if (calendarsCount > 1) {
            monthViews[1]?.alpha = 1f
            monthViews[monthViews.size - 2]?.alpha = 1f
        }
        monthViews.first()?.alpha = alpha
        monthViews.last()?.alpha = alpha
    }

    fun jumpNext() {
        if (canScrollToNext() && dragHelper.dragState == SimpleDragHelper.STATE_IDLE && dragHelper.animateSettle(0, 0, -monthViews[monthViews.size - 1]!!.measuredWidth - calendarSpace, 0)) {
            pendingChange = PENDING_NEXT
            invalidate()
        }
    }

    fun jumpPrevious() {
        if (canScrollToPrevious() && dragHelper.dragState == SimpleDragHelper.STATE_IDLE && dragHelper.animateSettle(0, 0, monthViews[0]!!.measuredWidth + calendarSpace, 0)) {
            pendingChange = PENDING_PREVIOUS
            invalidate()
        }
    }

    fun next(): Boolean {
        if (!canScrollToNext()) return false
        resetCalendarAlphas(1f)
        val tmp = monthViews[0]
        for (i in 0..monthViews.size - 2) {
            monthViews[i] = monthViews[i + 1]
        }
        monthViews[monthViews.size - 1] = tmp
        monthViews[monthViews.size - 2]?.let {
            prepareCalendar(tmp, it.year, it.month + 1)
        }
        syncArrows()
        resetCalendarAlphas(0F)
        requestLayout()
        invalidate()
        return true
    }

    fun previous(): Boolean {
        if (!canScrollToPrevious()) return false
        resetCalendarAlphas(1F)
        val tmp = monthViews[monthViews.size - 1]
        tmp?.alpha = 1f
        for (i in monthViews.size - 1 downTo 1) {
            monthViews[i] = monthViews[i - 1]
        }
        monthViews[0] = tmp
        monthViews[1]?.let {
            prepareCalendar(tmp, it.year, it.month - 1)
        }
        syncArrows()
        resetCalendarAlphas(0F)
        requestLayout()
        invalidate()
        return true
    }

    private fun syncArrows() {
        rightArrow?.isEnabled = canScrollToNext()
        leftArrow?.isEnabled = canScrollToPrevious()
    }

    private fun getThemeAccentColor(context: Context): Int {
        val colorAttr: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            android.R.attr.colorAccent
        } else {
            context.resources.getIdentifier("colorAccent", "attr", context.packageName)
        }
        val outValue = TypedValue()
        context.theme.resolveAttribute(colorAttr, outValue, true)
        return outValue.data
    }

    fun isDateEnabled(year: Int, month: Int, day: Int): Boolean {
        if (minDate != null && maxDate != null) return minDate!!.isLowerOrSame(year, month, day) && maxDate!!.isGraterOrSame(year, month, day)
        if (minDate != null) return minDate!!.isLowerOrSame(year, month, day)
        if (maxDate != null) return maxDate!!.isGraterOrSame(year, month, day)
        return true
    }

    private val dragHelperCallback = object : SimpleDragHelper.SimpleDragHelperCallback() {

        var initialHeight: Int? = null

        override fun onPositionChanged(changedView: View, dx: Int, dy: Int, idx: Int, idy: Int) {
            super.onPositionChanged(changedView, dx, dy, idx, idy)
            if (dx > 0 && !canScrollToPrevious()) {
                currentTranslationOffset = 0F
                dragHelper.forceSetDxDy(0, 0)
            } else if (dx < 0 && !canScrollToNext()) {
                currentTranslationOffset = 0F
                dragHelper.forceSetDxDy(0, 0)
            } else {
                currentTranslationOffset += idx
            }
            val range = getClampHorizontalDragRange(changedView, currentTranslationOffset, 0F)
            if (Math.abs(currentTranslationOffset) > range) {
                currentTranslationOffset = 0F
                dragHelper.forceSetDxDy(0, 0)
                if (idx > 0) previous() else next()
                initialHeight = null
            }
            val offsetPercent = minOf(1f, Math.abs(currentTranslationOffset) / range)
            if (currentTranslationOffset > 0) {
                monthViews.first()?.alpha = offsetPercent
                if (calendarsCount > 1) monthViews[monthViews.size - 2]?.alpha = 1f - offsetPercent
                monthViews.last()?.alpha = 0F
            } else {
                monthViews.first()?.alpha = 0F
                if (calendarsCount > 1) monthViews[1]?.alpha = 1f - offsetPercent
                monthViews.last()?.alpha = offsetPercent
            }
            leftArrow?.translationX = -currentTranslationOffset
            rightArrow?.translationX = -currentTranslationOffset
            if (isResizeAllowed) {
                val maxTargetHeight = getClampVerticalHeight(changedView, currentTranslationOffset, 0F)
                if (initialHeight == null) {
                    initialHeight = measuredHeight
                }
                if (maxTargetHeight != measuredHeight) {
                    layoutParams.height = (initialHeight!! + offsetPercent * (maxTargetHeight - initialHeight!!)).toInt()
                    layoutParams = layoutParams
                }
            }
            invalidate()
        }

        override fun onViewReleased(releasedChild: View, dx: Int, dy: Int, xvel: Float, yvel: Float) {
            super.onViewReleased(releasedChild, dx, dy, xvel, yvel)
            val horizontalRange = getClampHorizontalDragRange(releasedChild, currentTranslationOffset, 0F)
            if (currentTranslationOffset > 0 && (xvel > 0 || Math.abs(currentTranslationOffset) >= horizontalRange / 2)) {
                pendingChange = PENDING_PREVIOUS
                dragHelper.settleCapturedViewAt(currentTranslationOffset.toInt(), dy, horizontalRange, 0)
            } else if (currentTranslationOffset < 0 && (xvel < 0 || Math.abs(currentTranslationOffset) >= horizontalRange / 2)) {
                pendingChange = PENDING_NEXT
                dragHelper.settleCapturedViewAt(currentTranslationOffset.toInt(), dy, -horizontalRange, 0)
            } else {
                pendingChange = PENDING_NONE
                dragHelper.settleCapturedViewAt(currentTranslationOffset.toInt(), dy, 0, 0)
            }
            invalidate()
        }

        override fun onViewDragStateChanged(state: Int) {
            super.onViewDragStateChanged(state)
            if (state == SimpleDragHelper.STATE_IDLE) {
                currentTranslationOffset = 0F
                if (isResizeAllowed) {
                    layoutParams.height = initialLayoutParamsHeight!!
                    initialHeight = null
                }
                leftArrow?.translationX = 0F
                rightArrow?.translationX = 0F
                if (pendingChange == PENDING_NEXT) {
                    next()
                } else if (pendingChange == PENDING_PREVIOUS) {
                    previous()
                }
                pendingChange = PENDING_NONE
            } else if (state == SimpleDragHelper.STATE_DRAGGING) {
                pendingChange = PENDING_NONE
            }
        }

        override fun getViewVerticalDragRange(child: View, dx: Float, dy: Float): Int = 0

        fun getClampHorizontalDragRange(child: View, dx: Float, dy: Float): Int = monthViews[if (dx > 0) 0 else monthViews.size - 1]?.measuredWidth?.plus(calendarSpace)
                ?: 0

        fun getClampVerticalHeight(child: View, dx: Float, dy: Float): Int {
            val targetHeight = monthViews[if (dx > 0) 0 else monthViews.size - 1]?.measuredHeight
                    ?: 0
            var maxHeight = 0
            if (calendarsCount > 1) {
                val startOffset: Int = 2
                val endOffset: Int = monthViews.size - 2
                for (i in startOffset until endOffset) {
                    maxHeight = maxOf(maxHeight, monthViews[i]?.measuredHeight ?: 0)
                }
                if (dx > 0) {
                    maxHeight = maxOf(maxHeight, monthViews[1]?.measuredHeight ?: 0)
                } else {
                    maxHeight = maxOf(maxHeight, monthViews[monthViews.size - 2]?.measuredHeight
                            ?: 0)
                }
            }
            return maxOf(targetHeight, maxHeight) + paddingTop + paddingBottom
        }

    }

    override fun onSaveInstanceState(): Parcelable = SavedState(super.onSaveInstanceState()).also {
        it.currentYear = monthViews[1]?.year
        it.currentMonth = monthViews[1]?.month
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state !is SavedState) {
            super.onRestoreInstanceState(state)
            return
        }
        if (state.currentYear != null && state.currentMonth != null) {
            jumpToDate(state.currentYear!!, state.currentMonth!!)
        }
        super.onRestoreInstanceState(state.superState)
    }

    class SavedState : Parcelable {

        var superState: Parcelable? = null
        var currentYear: Int? = null
        var currentMonth: Int? = null

        constructor(superState: Parcelable?) {
            this.superState = superState
        }

        constructor(parcel: Parcel) {
            superState = parcel.readParcelable(null)
            currentYear = parcel.readValue(Int::class.java.classLoader) as? Int
            currentMonth = parcel.readValue(Int::class.java.classLoader) as? Int
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeParcelable(superState, flags)
            parcel.writeValue(currentYear)
            parcel.writeValue(currentMonth)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<SavedState> {
            override fun createFromParcel(parcel: Parcel): SavedState {
                return SavedState(parcel)
            }

            override fun newArray(size: Int): Array<SavedState?> {
                return arrayOfNulls(size)
            }
        }

    }

}