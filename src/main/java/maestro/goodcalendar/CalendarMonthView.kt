package maestro.goodcalendar

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Path
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import android.widget.TextView
import java.util.*

/**
 * Created by maestro123 on 4/19/2018.
 */
class CalendarMonthView : ViewGroup {

    val dayViews = mutableListOf<CheckedTextView>()
    val weekDayTitleViews = mutableListOf<TextView>()

    var year = -1
        private set
    var month = -1
        private set
    var firstDayWeekOffset = 0
        private set
    var maxDaysInMonth = 0
        private set

    lateinit var monthTitleView: TextView

    private val calendar = GregorianCalendar()

    private var isLayoutBlocked = false

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    fun setup(parent: GoodCalendar, year: Int, month: Int) {
        calendar.set(year, month, 1)
        this.year = calendar.get(Calendar.YEAR)
        this.month = calendar.get(Calendar.MONTH)
        firstDayWeekOffset = calendar.get(GregorianCalendar.DAY_OF_WEEK) - 1

        maxDaysInMonth = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
        monthTitleView.text = parent.formatter.formatMonthTitle(calendar.time)
        dayViews.forEachIndexed { index, view ->
            if (index < maxDaysInMonth) {
                view.text = (index + 1).toString()
                if (view.isChecked) {
                    view.isChecked = false
                }
                view.isChecked = parent.mode.isChecked(year, month, index + 1)
                view.isEnabled = parent.isDateEnabled(year, month, index + 1)
                view.visibility = View.VISIBLE
            } else {
                view.text = ""
                view.isEnabled = false
                view.isChecked = false
                view.visibility = View.GONE
            }
        }
    }

    fun prepare(parent: GoodCalendar) {
        isLayoutBlocked = true
        addView(instantiateMonthTitleView(parent.monthTitleLayout).also {
            monthTitleView = it
        })

        (0 until 7).forEach {
            addView(instantiateWeekDayTitleView(parent.weekDayTitleLayout).also {
                weekDayTitleViews.add(it)
            })
        }

        (0 until 31).forEachIndexed { index, item ->
            addView(instantiateDayView(parent.dayLayout).also {
                it.setOnClickListener { view ->
                    parent.mode.onDayCheckChange(year, month, index + 1, it.isChecked)
                }
                if (it is CalendarDayView) {
                    it.prepareBackground(parent.tintColor)
                }
                dayViews.add(it)
            })
        }
        prepareWeekDayTitles(parent)

        isLayoutBlocked = false
        requestLayout()
        invalidate()
    }

    @SuppressLint("MissingSuperCall")
    override fun requestLayout() {
        if (!isLayoutBlocked) {
            super.requestLayout()
        }
    }

    override fun invalidate() {
        if (!isLayoutBlocked) {
            super.invalidate()
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (width == 0 || childCount == 0) return

        val left = paddingLeft
        var right = paddingRight
        var top = paddingTop

        val cellStep = width / 7

        val params = monthTitleView.layoutParams as CalendarMonthParams
        val gravity = GravityCompat.getAbsoluteGravity(params.gravity, ViewCompat.getLayoutDirection(this))
        when (gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
            Gravity.CENTER_HORIZONTAL -> {
                monthTitleView.layout(width / 2 - monthTitleView.measuredWidth / 2, top, width / 2 + monthTitleView.measuredWidth / 2, top + monthTitleView.measuredHeight)
            }
            Gravity.RIGHT,
            GravityCompat.END -> {
                monthTitleView.layout(paddingRight - monthTitleView.measuredWidth, top, paddingRight, top + monthTitleView.measuredHeight)
            }
            else -> {
                monthTitleView.layout(left, top, left + monthTitleView.measuredWidth, top + monthTitleView.measuredHeight)
            }
        }
        top += monthTitleView.measuredHeight

        (parent as? GoodCalendar)?.let {
            top += it.monthTitleSpace
        }

        var maxHeight = 0
        weekDayTitleViews.forEachIndexed { index, item ->
            item.layout(left + cellStep * index, top, left + cellStep * index + item.measuredWidth, top + item.measuredHeight)
            maxHeight = Math.max(maxHeight, item.measuredHeight)
        }
        top += maxHeight

        (parent as? GoodCalendar)?.let {
            top += it.weekDaysTitleSpace
        }

        maxHeight = 0
        var currentRow = -1
        dayViews.forEachIndexed { index, item ->
            if (item.visibility != View.GONE) {
                val index = (firstDayWeekOffset + index)
                val row = index / 7
                if (currentRow != row) {
                    top += maxHeight
                    currentRow = row
                    maxHeight = maxOf(maxHeight, item.measuredHeight)
                }
                item.layout(left + cellStep * (index % 7), top, left + cellStep * (index % 7) + item.measuredWidth, top + item.measuredHeight)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val availableWidth = MeasureSpec.getSize(widthMeasureSpec)
        val availableHeight = MeasureSpec.getSize(heightMeasureSpec)

        var preferredWidth = 0
        if (availableWidth > 0) {
            preferredWidth = (availableWidth - paddingLeft - paddingRight) / 7
        }

        var width = 0
        var height = 0
        var currentRow = -1
        var maxChildHeight = 0
        for (i in 0 until dayViews.size) {
            val view = dayViews[i]
            if (view.visibility == View.GONE) continue
            measureChildWithPreferredSize(view, widthMeasureSpec, heightMeasureSpec, preferredWidth)
            val row = (i + firstDayWeekOffset) / 7
            if (currentRow != row && view.measuredHeight > 0) {
                currentRow = row
                height += view.measuredHeight
                maxChildHeight = maxOf(maxChildHeight, view.measuredHeight)
            }
        }
        weekDayTitleViews.forEachIndexed { index, view ->
            measureChildWithPreferredSize(view, widthMeasureSpec, heightMeasureSpec, preferredWidth, 0)
            if (index == 0) {
                height += view.measuredHeight
            }
            width += view.measuredWidth
        }

        measureChildWithPreferredSize(monthTitleView, widthMeasureSpec, heightMeasureSpec, availableWidth, 0)
        height += monthTitleView.measuredHeight

        (parent as? GoodCalendar)?.let {
            height += it.monthTitleSpace + it.weekDaysTitleSpace
            if ((!it.isResizeAllowed || MeasureSpec.getMode(it.initialHeightMode!!) == MeasureSpec.EXACTLY) && currentRow + 1 < 6) {
                height += maxChildHeight
            }
        }

        setMeasuredDimension(maxOf(availableWidth, width), height)
    }

    private fun measureChildWithPreferredSize(child: View, widthMeasureSpec: Int, heightMeasureSpec: Int, preferredWidth: Int, preferredHeight: Int = preferredWidth) {
        val params = child.layoutParams
        val widthSpec = if (preferredWidth > 0) {
            MeasureSpec.makeMeasureSpec(preferredWidth, MeasureSpec.EXACTLY)
        } else {
            getChildMeasureSpec(widthMeasureSpec, 0, params.width)
        }
        val heightSpec = if (preferredHeight > 0) {
            MeasureSpec.makeMeasureSpec(preferredHeight, MeasureSpec.EXACTLY)
        } else {
            getChildMeasureSpec(heightMeasureSpec, 0, params.height)
        }
        child.measure(widthSpec, heightSpec)
    }

    override fun checkLayoutParams(p: LayoutParams?): Boolean = p is CalendarMonthParams

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams = CalendarMonthParams(p)

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams = CalendarMonthParams(context, attrs)

    override fun generateDefaultLayoutParams(): LayoutParams = CalendarMonthParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER)

    fun syncCheckedState(calendarView: GoodCalendar) {
        dayViews.forEachIndexed { index, item ->
            if (item.isChecked) {
                item.isChecked = false
            }
            item.isChecked = item.isEnabled && calendarView.mode.isChecked(year, month, index + 1)
            item.isEnabled = calendarView.isDateEnabled(year, month, index + 1)
        }
    }

    fun updateTintColor(calendarView: GoodCalendar, color: Int) {
        dayViews.forEach {
            if (it is CalendarDayView) {
                it.prepareBackground(color)
            }
        }
    }

    private fun prepareWeekDayTitles(parent: GoodCalendar) {
        weekDayTitleViews.forEachIndexed { index, view ->
            view.text = parent.formatter.formatWeekDayTitle(index + 1)
        }
    }

    private fun instantiateDayView(layoutRes: Int): CheckedTextView {
        return if (layoutRes != -1) LayoutInflater.from(context).inflate(layoutRes, this, false) as CheckedTextView else CalendarDayView(context)
    }

    private fun instantiateMonthTitleView(layoutRes: Int): TextView {
        return if (layoutRes != -1) LayoutInflater.from(context).inflate(layoutRes, this, false) as TextView else CalendarMonthTitleView(context)
    }

    private fun instantiateWeekDayTitleView(layoutRes: Int): TextView {
        return if (layoutRes != -1) LayoutInflater.from(context).inflate(layoutRes, this, false) as TextView else CalendarWeekDayTitleView(context)
    }

    fun isNextChecked(view: View): Boolean = isNextChecked(dayViews.indexOf(view))
    fun isNextChecked(index: Int) = index < maxDaysInMonth - 1 && (parent as GoodCalendar).mode.isChecked(year, month, index + 2)

    fun isPreviousChecked(view: View): Boolean = isPreviousChecked(dayViews.indexOf(view))
    fun isPreviousChecked(index: Int) = index > 0 && (parent as GoodCalendar).mode.isChecked(year, month, index)

    fun isFirstInRow(view: View): Boolean = isFirstInRow(dayViews.indexOf(view))
    fun isFirstInRow(index: Int) = (firstDayWeekOffset + index) % 7 == 0

    fun isLastInRow(view: View): Boolean = isLastInRow(dayViews.indexOf(view))
    fun isLastInRow(index: Int) = (firstDayWeekOffset + index) % 7 == 6

    fun isTopChecked(view: View): Boolean = isTopChecked(dayViews.indexOf(view))
    fun isTopChecked(index: Int) = index - 7 >= 0 && (parent as GoodCalendar).mode.isChecked(year, month, index - 6)

    fun isBottomChecked(view: View): Boolean = isBottomChecked(dayViews.indexOf(view))
    fun isBottomChecked(index: Int) = index + 7 < maxDaysInMonth && (parent as GoodCalendar).mode.isChecked(year, month, index + 8)

    fun buildDayPath(view: View, path: Path) {
        val index = dayViews.indexOf(view)

        val nextChecked = isNextChecked(index)
        val previousChecked = isPreviousChecked(index)
        val firstInRow = isFirstInRow(index)
        val lastInRow = isLastInRow(index)
        val topChecked = isTopChecked(index)
        val bottomChecked = isBottomChecked(index)

        if (previousChecked && !firstInRow) {
            path.addRect(0F, 0F, (view.width / 2).toFloat(), view.height.toFloat(), Path.Direction.CW)
        }
        if (nextChecked && !lastInRow) {
            path.addRect((view.width / 2).toFloat(), 0F, view.width.toFloat(), view.height.toFloat(), Path.Direction.CW)
        }
        if (topChecked) {
            path.addRect(0F, 0F, view.width.toFloat(), (view.height / 2).toFloat(), Path.Direction.CW)
        }
        if (bottomChecked) {
            path.addRect(0F, (view.height / 2).toFloat(), view.width.toFloat(), view.height.toFloat(), Path.Direction.CW)
        }

    }

    open class CalendarMonthParams : LayoutParams {

        var gravity = -1

        constructor(width: Int, height: Int, gravity: Int) : super(width, height) {
            this.gravity = gravity
        }

        constructor(source: LayoutParams?) : super(source) {
            if (source is CalendarMonthParams) {
                gravity = source.gravity
            }
        }

        constructor(c: Context, attrs: AttributeSet?) : super(c, attrs) {
            val a = c.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.layout_gravity))
            gravity = a.getInt(0, gravity)
            a.recycle()
        }

    }

}